console.log("Postman and REST API Activity")

// https://jsonplaceholder.typicode.com/todos

// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

let json = ''
async function fetchData() {
	
	let result = await fetch('https://jsonplaceholder.typicode.com/todos')
	// console.log('raw data')
	// console.log(result)
	// console.log(typeof result)

	// converts the data from the response
	json = await result.json()
	// console.log('converted to object')
	console.log(json)
	// console.log(typeof json)
}

fetchData()


// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.


fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())//parse the response as JSON
.then(data => {


// console.log(data)


let mapTitle = data.map(
		function(toDoList) {
			
			return `${toDoList.id} : ${toDoList.title}`
			
		}
	)


mapTitle.forEach((toDoList) => {
	console.log(toDoList)
})

})//process the result of fetch


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())//parse the response as JSON
.then(data => {console.log(data)})//process the result of fetch


// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch('https://jsonplaceholder.typicode.com/todos/')
.then(response => response.json())//parse the response as JSON
.then(data => {
	data.forEach ((toDoList) => {
			console.log(toDoList.id , toDoList.title , toDoList.completed)
	})
})//process the result of fetch


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API. 

fetch('https://jsonplaceholder.typicode.com/todos', {
	method :'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Step7 New Post",
		complete : true,
		userId: 2
	})
})
.then(resPost => resPost.json())//parse the response as JSON
.then(dataPost => {console.log(dataPost)})//process the result of fetch


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API. 

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method :'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Step8 Update Post",
		completed : true,
		userId: 1
	})
})
.then(resPut => resPut.json())//parse the response as JSON
.then(dataPut => {console.log(dataPut)})//process the result of fetch


// 9. Update a to do list item by changing the data structure to contain the following properties: - Title - Description - Status - Date Completed - User ID 

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method :'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Step9 Change Data Structure",
		description: "Update a to do list item by changing the data structure to contain the following properties: - Title - Description - Status - Date Completed - User ID",
		status: "Completed",
		dateCompleted:"December 3, 2021",
		userId: 2
	})
})
.then(resPut_9 => resPut_9.json())//parse the response as JSON
.then(dataPut_9 => {console.log(dataPut_9)})//process the result of fetch



// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API. 


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method :'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: "Step10 Patch method",
		userId: 1
	})
})
.then(resPatch => resPatch.json())//parse the response as JSON
.then(dataPatch => {console.log(dataPatch)})//process the result of fetch



// 11. Update a to do list item by changing the status to complete and add a date when the status was changed. 

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method :'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		completed : true,
		dateCompleted:"December 3, 2021",
		userId: 2
	})
})
.then(resPut_11 => resPut_11.json())//parse the response as JSON
.then(dataPut_11 => {console.log(dataPut_11)})//process the result of fetch



// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API. 

fetch('https://jsonplaceholder.typicode.com/todos/1', {method :'DELETE'})
.then(resDelete => resDelete.json())//parse the response as JSON
.then(dataDelete => {console.log(dataDelete)})//process the result of fetch