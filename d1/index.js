// for REST API TOPIC
// JAVASCRIPT SYNCHRONOUS VS ASYNCHRONOUS


// SYNCHRONOUS JAVASCRIPT
// default setting
// executes one code/statement at a time
// will only move to the next code once the first code is completed


console.log("Hello World")
// console.log("Hello Again")

/*for (let i = 0; i <= 1500; i++){
	console.log(i)

}*/

// Blocking is when the execution of additional JS process must wait until the operation completes.
// runs the code from top to bottom
console.log("Goodbye")



// ASYNCHRONOUS JAVASCRIPT

// 2 primary triggers
// 1. Browser API/WEB  API events or functions. These include methods like setTimeout, or event handlers onclick, mouse over, scroll and many more.

/*function printME(){
	console.log('print Me')
}


setTimeout(printME,5000)


// another async sample

function print() {
	console.log('print meee')
}

function test(){
	console.log('test')
}


// setTimeout(print,4000)
print()
test()


// another async sample

function f1(){
	console.log('f1')
}

function f2(){
	console.log('f2')
}


function main(){
	console.log('main')
	setTimeout(f1,0)

	new Promise((resolve,reject) => resolve('I am a promise')).then(resolve => console.log(resolve))

	f2()
}

main();
*/
// call back queue
// QUEUE (Data Structure)
// FIFO first-in-first-out


// JOB QUEUE
// Every time a promise occurs in the code the executor function gets into the job queue. The event loop works as usual to look into the queues but gives priority to the job queue items over the callback queue items when the stack is free.


// 2. Promises. A unique javascript object that allows us to perform asynchronous behavior


// the promises object represents the eventual completion or failure of an asynchronous operation and its resulting value

/*new Promise((resolve,reject) => resolve('I am a promise')).then(resolve => console.log(resolve))*/

// REST API Using a JSONPLACEHOLDER
// https://jsonplaceholder.typicode.com/posts

/*
	Getting all posts (GET Method)

	Fetch - allows us to asynchronously request for a resource (data)

	we used the promise for async

	syntax:

		fetch('<url/endpoint/rout>',{optional object})
		.then(response => response.json)
		.then(data)


		optional object : method, content-type

*/


// fetch sample (synchronous):

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())//parse the response as JSON
.then(data => {console.log(data)})//process the result of fetch


// async await function

async function fetchData() {
	// wait for the 'fetch' method to complete then stores the value in a variable name

	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log('raw data')
	console.log(result)
	console.log(typeof result)

	// converts the data from the response
	let json = await result.json()
	console.log('converted')
	console.log(json)
	console.log(typeof json)
}

fetchData()


// Retrieving a specific post

console.log('retrieving a specific post')
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then(response => response.json())//parse the response as JSON
.then(data => {console.log(data)})//process the result of fetch


// creates a new post

console.log('creates a new post')
fetch('https://jsonplaceholder.typicode.com/posts', {
	method :'POST',
	headers: {
		'Content-Type':'application/json'
	},
	//sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title: "new post",
		body: "hello again",
		userId: 2
	})


})
.then(res => res.json())//parse the response as JSON
.then(data => {console.log(data)})//process the result of fetch


// updates a post (PUT whole resource PATCH partial)

// PUT - will return what was updated. PATCH will return the whole resource with updat

console.log('update a post')
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method :'PUT',
	// PUT method - updates the whole document
	headers: {
		'Content-Type':'application/json'
	},
	//sets the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title: "update post",
		body: "hello i updated this",
		userId: 1
	})


})
.then(res => res.json())//parse the response as JSON
.then(data => {console.log(data)})//process the result of fetch



// DELETE post

console.log('delete post')
fetch('https://jsonplaceholder.typicode.com/posts/1', {	method :'DELETE'	})



.then(res => res.json())//parse the response as JSON
.then(data => {console.log(data)})//process the result of fetch



// FILTERING POSTS
console.log('filter post')
fetch('https://jsonplaceholder.typicode.com/posts/?userId=1')
// add question mark (?) to denote start of filter parameters.
// filter should be available in the data
// for multiple: paramA=valueA&paramB=valueB
.then(res => res.json())//parse the response as JSON
.then(data => {console.log(data)})//process the result of fetch


// retrieving nested/related comments to posts
// retrieving comments for a specific post (/posts/:id/comments)

console.log('nested post')
fetch('https://jsonplaceholder.typicode.com/posts/2/comments')

.then(res => res.json())//parse the response as JSON
.then(data => {console.log(data)})//process the result of fetch